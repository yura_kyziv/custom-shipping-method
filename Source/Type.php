<?php
declare(strict_types=1);

namespace Mastering\CustomShippingMethod\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mastering\CustomShippingMethod\Api\Data\TypeInterface;

class Type implements OptionSourceInterface
{

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => TypeInterface::PER_ORDER_VALUE, 'label' => __(TypeInterface::PER_ORDER_LABEL)],
            ['value' => TypeInterface::PER_ITEM_VALUE, 'label' => __(TypeInterface::PER_ITEM_LABEL)],
        ];
    }
}
