<?php
declare(strict_types=1);

namespace Mastering\CustomShippingMethod\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mastering\CustomShippingMethod\Api\Data\CalculateHandlingFeeInterface;

class CalculateHandlingFee implements OptionSourceInterface
{

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => CalculateHandlingFeeInterface::PERCENT_VALUE, 'label' => __(CalculateHandlingFeeInterface::PERCENT)],
            ['value' => CalculateHandlingFeeInterface::FIXED_VALUE, 'label' => __(CalculateHandlingFeeInterface::FIXED)],
        ];
    }
}

