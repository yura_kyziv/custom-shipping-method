<?php

namespace Mastering\CustomShippingMethod\Api\Data;

interface CalculateHandlingFeeInterface
{
    const FIXED = 'Fixed';
    const PERCENT = 'Percent';

    const FIXED_VALUE = 'F';
    const PERCENT_VALUE = 'P';

}