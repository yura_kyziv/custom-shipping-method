<?php

namespace Mastering\CustomShippingMethod\Api\Data;

interface TypeInterface
{
    const PER_ORDER_LABEL = 'Per Order';
    const PER_ITEM_LABEL = 'Per Item';

    const PER_ORDER_VALUE = 'O';
    const PER_ITEM_VALUE = 'I';
}